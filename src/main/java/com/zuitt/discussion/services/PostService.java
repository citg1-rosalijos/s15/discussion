package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    // Create post
    void createPost(String stringToken, Post post);

    // Read all posts
    Iterable<Post> getPosts();

    // Update post
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // Delete post
    ResponseEntity deletePost(Long id, String stringToken);

    Iterable<Post> getMyPosts(String stringToken);

}
