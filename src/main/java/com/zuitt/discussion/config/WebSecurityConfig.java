package com.zuitt.discussion.config;

import com.zuitt.discussion.services.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticate jwtAuthenticate;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public JwtAuthenticate jwtAuthenticationEntryPointBean() throws Exception {
        return new JwtAuthenticate();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    // routes that will not require JWT Token
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // This enables the CORS and disabling CSRF protection in the Spring Security of the application.
        // The CSRF is disabled because were using JWT on verifying the web requests.
        // CORS: allows web browsers to make cross-origin HTTP requests from one domain to another domain.
        // CSRF: is an attack that tricks a user into performing an action on a different website without their consent.
        httpSecurity.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/authenticate").permitAll() // does not require authentication
                .antMatchers("/users/register").permitAll() // does not require authentication
                .antMatchers(HttpMethod.GET, "/posts").permitAll() // does not require authentication
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll() // does not require authentication
                .anyRequest().authenticated().and() // requires authentication
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticate).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS); // application do not use any sessions

        // This is used to add a custom filter (JwtRequestFilter) before a specific filter class (UsernamePasswordAuthenticationFilter)
        // "jwtRequestFilter" is responsible for handling JWT authentication and authorization logic.
        // "UsernamePasswordAuthenticationFilter" is a built-in filter in Spring Security that handles form-based authentication using username and password.
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
